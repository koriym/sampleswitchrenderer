<?php

namespace Kumamidori\SampleSwitchRenderer\Resource\Page;

use Kumamidori\SampleSwitchRenderer\Test\ContextAwareTestCase;

class IndexTest extends ContextAwareTestCase
{
    public function testOnGetReturnsCode()
    {
        $resource = clone $GLOBALS['RESOURCE'];
        $page = $resource->get->uri('page://self/index')->eager->request();

        $this->assertSame(200, $page->code);
    }

    public function testOnGetReturnsViewCaseContextHtmlApp()
    {
        $app = $this->getApp('html-app');
        $page = $app->resource->get->uri('page://self/index')->eager->request();
        $expectedView = <<<EOD
html: &lt;h1&gt;Hello BEAR.Sunday&lt;/h1&gt;
EOD;
        $this->assertSame($expectedView, (string) $page);
    }
}
