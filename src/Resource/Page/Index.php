<?php

namespace Kumamidori\SampleSwitchRenderer\Resource\Page;

use BEAR\Resource\ResourceObject;
use BEAR\Sunday\Inject\ResourceInject;

class Index extends ResourceObject
{
    use ResourceInject;

    public function onGet()
    {
        $page = $this->resource->get->uri('page://self/samplehtml')
            ->eager->request();

        $this->body = [
            'html' => $page,
        ];

        return $this;
    }
}
