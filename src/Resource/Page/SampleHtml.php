<?php

namespace Kumamidori\SampleSwitchRenderer\Resource\Page;

use BEAR\Resource\RenderInterface;
use BEAR\Resource\ResourceObject;
use Ray\Di\Di\Inject;
use Ray\Di\Di\Named;

class SampleHtml extends ResourceObject
{
    /**
     * @Inject
     * @Named("html")
     */
    public function setRenderer(RenderInterface $renderer)
    {
        parent::setRenderer($renderer);
    }

    public function onGet($name = 'BEAR.Sunday')
    {
        $this->body['greeting'] = 'Hello ' . $name;

        return $this;
    }
}
