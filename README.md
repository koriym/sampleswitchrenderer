# 【WIP】 リソース単位でレンダラーを変更するサンプル

[![wercker status](https://app.wercker.com/status/f8103ec70d0d41f1e76529a7ed63e41e/s/ "wercker status")](https://app.wercker.com/project/byKey/f8103ec70d0d41f1e76529a7ed63e41e)


![2017-04-24_14_26_28.png](https://bitbucket.org/repo/rpp8yr6/images/2114887933-2017-04-24_14_26_28.png)

ref. https://github.com/bearsunday/BEAR.Sunday/issues/104

## やりたいこと

コンテキスト `app` の `Api` リソースのGET処理内で、`SampleHtml` リソースをGETし、設定します。

`Api` リソース -> `SampleHtml` リソース

このとき `SampleHtml` リソースのHTMLレンダリング結果を `Api` リソースの情報として設定したいです。

```
{
  "html": "<h1>（省略...）</h1>"
}
```

## 現状の挙動

HTMLレンダリング結果ではなくJSONが設定されます。
```
{
  "html": {"greeting":"（省略...）"}
}
```
